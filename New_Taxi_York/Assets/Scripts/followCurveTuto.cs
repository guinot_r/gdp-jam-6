﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class followCurveTuto : MonoBehaviour {
	
	public ManagerScripts MS;
	public int _speed = 5;
	public float _lengthTimeBoost = 3F;
	public int _speedDuringBoost = 8;
	private int _oldSpeed = 0;
	private float _timer = 0;
	private bool _boost = false;
	private float oldX = 1F;
	// va bouger la cam et le joueur  // OK
	// la camera aura un itween path vide // OK
	// chaque tile aura un script et des assets associé et sera sur la scene
	// on bougera simplement le tile à la bonne pos et on settera le curve de la camera 
	//mettre uin script pour bouger les points du curve selon le gameobj
	void Start()
	{
		int i = 0;
		_oldSpeed = _speed;
		iTween.MoveTo (gameObject, iTween.Hash ("path", iTweenPath.GetPath ("TaxiCurve"),"speed", _speed, "easetype", "linear", "onComplete", "onMoveComplete"));
		
		//iTween.MoveTo (Camera.main, iTween.Hash ("path", iTweenPath.GetPath ("TaxiCurve"), "time", 5));
	}
	
	void onMoveComplete()
	{
		MS.playerWin ();
		Application.LoadLevel("Menu");


		Debug.Log ("FINISH");

		
		
	}
	
	void Update()
	{
		if (gameObject.transform.position.x - oldX > 0) {
						GameObject.Find ("Taxi").transform.localScale = new Vector3 (1, GameObject.Find ("Taxi").transform.localScale.y, GameObject.Find ("Taxi").transform.localScale.z);

						Debug.Log ("FLIP");
		} else {
			GameObject.Find ("Taxi").transform.localScale = new Vector3 (- 1, GameObject.Find ("Taxi").transform.localScale.y, GameObject.Find ("Taxi").transform.localScale.z);

		}

		if (_boost == true) {

			_timer += Time.deltaTime;
			
			if (_timer > _lengthTimeBoost)
			{

				_timer = 0;
				_boost = false;
				MS.setIsBoost(_boost);
			}
		}
		if (MS.getIsCarbuMax ()) {


			_boost = true;
			MS.setIsBoost(_boost);
			_timer = 0;
		}
		oldX = gameObject.transform.position.x;
	}
	
	
	
}
