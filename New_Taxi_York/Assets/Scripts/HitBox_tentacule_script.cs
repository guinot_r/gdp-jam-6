﻿using UnityEngine;
using System.Collections;

public class HitBox_tentacule_script : MonoBehaviour {
	public ManagerScripts MS;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other){
		if(MS._gameState != gameState.Lose){
			if((other.tag == "ClientA" || other.tag == "ClientB" || other.tag == "ClientC" || other.tag == "ClientD") && MS._gameState != gameState.Lose){
				other.gameObject.GetComponent<ClientBehaviour>().setFall(true);
			}

			if(other.tag == "Obstacle" && MS._gameState != gameState.Lose){
				MS.playerLose();
			}
		}
	}

	void OnTriggerStay2D(Collider2D other){
		int count=0;
		foreach(string s in Input.GetJoystickNames()){
			count++;
		}
		if(count > 0){//si le joystick est activer
			if(Input.GetKey(KeyCode.JoystickButton0) && (other.tag == "ClientA" || other.tag == "ClientB" || other.tag == "ClientC" || other.tag == "ClientD") && MS._gameState != gameState.Lose){
				MS.updateCarburant();
				if(other.gameObject.GetComponent<ClientBehaviour>() != null)
				other.gameObject.GetComponent<ClientBehaviour>().setCliked(true);
			}
		}else{//avec la souris
			if(Input.GetMouseButton(0) && (other.tag == "ClientA" || other.tag == "ClientB" || other.tag == "ClientC" || other.tag == "ClientD") && MS._gameState != gameState.Lose){
				MS.updateCarburant();
				if(other.gameObject.GetComponent<ClientBehaviour>() != null)
				other.gameObject.GetComponent<ClientBehaviour>().setCliked(true);
			}
		}
	}
}
