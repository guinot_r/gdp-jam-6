﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class MovePos : MonoBehaviour {

	public  TypePath _type;
	private iTweenPath[] _listPath;

	private Vector3[] initialNodePos;

	// Use this for initialization
	public void ChangePos(Vector3 pos)
	{
		gameObject.transform.localPosition = pos;

		iTweenPath t;
		//Camera.main.transform.position = new Vector3 (pos.x, pos.y, -2);
		//DestroyImmediate (Camera.main.GetComponent<iTweenPath> ().gameObject, true);

		//iTweenPath it;
		//it = Camera.main.gameObject.AddComponent("iTweenPath") as iTweenPath;
		t = Camera.main.GetComponent<iTweenPath>();
		t.transform.position = pos;
//		t.SetNodes(this.GetComponentsInChildren<iTweenPath> ()[0].nodes);

		//DestroyImmediate(Camera.main.GetComponent<iTweenPath>().gameObject, true);
		 //(this.GetComponentInChildren<iTweenPath> ());

	}

	void Start () {

		_listPath = this.GetComponentsInChildren<iTweenPath> ();

		initialNodePos = new Vector3[_listPath [0].nodeCount]; // size de node

		for (int i = 0; i < _listPath[0].nodeCount; i++) {
			initialNodePos[i] = _listPath[0].nodes[i];
		}
	}
	
	// Update is called once per frame
	void Update () {

		foreach (var path in _listPath) 
		{
			for (int i = 0; i < _listPath[0].nodeCount; i++)
			{
				path.nodes[i] =  initialNodePos[i] + this.transform.localPosition;

			}
		}
	
	}
}
