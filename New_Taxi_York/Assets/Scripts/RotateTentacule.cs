﻿using UnityEngine;
using System.Collections;

public class RotateTentacule : MonoBehaviour {

	public GameObject tentaculePivot;


	private Vector3 coordTentacule;
	private float currentAngle;

	// Use this for initialization
	void Start () {
		coordTentacule = tentaculePivot.transform.position;
		currentAngle = this.transform.rotation.z;
	}

	private Vector2 mouseVector(){

		int count = 0;
		foreach(string s in Input.GetJoystickNames())
			count++;

		if(count == 0){
			return new Vector2(Input.mousePosition.x - (Screen.width/2),
		                   	   Input.mousePosition.y - (Screen.height/2));
		}else{
			if(Input.GetKey(KeyCode.Joystick1Button0))
				Debug.Log("Bouton A appuyé");
			return new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
		}
	}

	private Vector2 tentaculeVector(){
		coordTentacule = tentaculePivot.transform.position;
		return new Vector2(coordTentacule.x - this.transform.position.x,
		                   coordTentacule.y - this.transform.position.y);
	}

	// Update is called once per frame
	void Update () {
		Vector2 mVect = mouseVector();
		Vector2 tVect = tentaculeVector();
		float angleBetwMaT;
		float desiredAngle = Vector2.Angle(Vector2.right, mVect);
		if(Vector3.Cross(Vector2.right, mVect).z < 0)//condition pour savoir si on est en présence d'un angle obtus
			desiredAngle = 180 + (180 - desiredAngle);//dans ce cas la on ajoute 180 degres

		angleBetwMaT = Vector2.Angle(tVect , mVect);
		if(Vector3.Cross(tVect , mVect).z < 0)
			angleBetwMaT = 180 + (180 - angleBetwMaT);

		if(currentAngle != desiredAngle){
			currentAngle = desiredAngle;
			this.transform.Rotate(Vector3.forward, angleBetwMaT);
		}

	
	}



}
