﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class followCurve : MonoBehaviour {


	private MovePos		 _curLevel = null;
	private bool 		_isOK = false;
	private float		_timer = 0.0F;
	private int			_nbTiles = 0;
	private List<MovePos> 	_tilesSN = new List<MovePos>();
	private List<MovePos> 	_tilesSE = new List<MovePos>();
	private List<MovePos> 	_tilesON = new List<MovePos>();
	private List<MovePos> 	_tilesEN = new List<MovePos>();
	private List<MovePos> 	_tilesEO = new List<MovePos>();
	private List<MovePos> 	_tilesOE = new List<MovePos>();
	private List<MovePos> 	_tilesSO = new List<MovePos>();

	// va bouger la cam et le joueur  // OK
	// la camera aura un itween path vide // OK
	// chaque tile aura un script et des assets associé et sera sur la scene
	// on bougera simplement le tile à la bonne pos et on settera le curve de la camera 
	//mettre uin script pour bouger les points du curve selon le gameobj
	void Start()
	{
		int i = 0;

		foreach (MovePos mv in GameObject.Find("Tiles").GetComponentsInChildren<MovePos>()) {

			if (mv._type == TypePath.SN)
				_tilesSN.Add(mv);
			else if (mv._type == TypePath.SE)
				_tilesSE.Add(mv);
			else if (mv._type == TypePath.ON)
				_tilesON.Add(mv);
			else if (mv._type == TypePath.EN)
				_tilesEN.Add(mv);
			else if (mv._type == TypePath.EO)
				_tilesEO.Add(mv);
			else if (mv._type == TypePath.OE)
				_tilesOE.Add(mv);
			else if (mv._type == TypePath.EO)
				_tilesEO.Add(mv);
			else if (mv._type == TypePath.SO)
				_tilesSO.Add(mv);

			mv.gameObject.SetActive(false);
			i++;
		}
		_nbTiles = i;
		iTween.MoveTo (gameObject, iTween.Hash ("path", iTweenPath.GetPath ("TaxiCurve"),"speed", 5, "easetype", "linear", "onComplete", "onMvtComplete"));

		//iTween.MoveTo (Camera.main, iTween.Hash ("path", iTweenPath.GetPath ("TaxiCurve"), "time", 5));
	}

	void onMvtComplete()
	{
		// appeller le script qui fera bouger le prochain tile / path
		//if (_curLevel.gameObject
		Debug.Log ("FUCK");

		iTween.Stop ();
		if (_curLevel != null) { 
			_curLevel.gameObject.SetActive(false);
		}
		int level = 0;
		MovePos sc;
		//if (_curLevel.gameObjec
		/*if (_curLevel._type == TypePath.EN) {
			level = Random.Range(0, _tilesEN.Count);
			sc = _tilesEN [level];
		}
		else if (_curLevel._type == TypePath.EO) {
			level = Random.Range(0, _tilesEO.Count);
			sc = _tilesEO [level];
		}
		else if (_curLevel._type == TypePath.OE) {
			level = Random.Range(0, _tilesOE.Count);
			sc = _tilesOE [level];
		}
		else if (_curLevel._type == TypePath.ON) {
			level = Random.Range(0, _tiles.Count);
			sc = _tiles [level];
		}
		else if (_curLevel._type == TypePath.EN) {
			level = Random.Range(0, _tilesEN.Count);
			sc = _tiles [level];
		}
		else if (_curLevel._type == TypePath.EN) {
			level = Random.Range(0, _tilesEN.Count);
			sc = _tiles [level];
		}*/

		level = Random.Range(0, _tilesSN.Count);
		sc = _tilesSN [level];
		_curLevel = sc;
		sc.gameObject.SetActive (true);	
		sc.ChangePos (gameObject.transform.position);//ca marche o/
		Debug.Log ("NEW START : " + gameObject.transform.position);
		// faut check si l'avant = EO et l'après = OE
		_isOK = true;
		_timer = 0.0F;


	}

	void Update()
	{
		if (_isOK == true) {
			_timer += Time.deltaTime;
			if (_timer > 0.1F)
			{

				iTween.MoveTo (gameObject, iTween.Hash ("path", iTweenPath.GetPath ("TaxiCurve"), "time", 5,"easetype", "linear", "onComplete", "onMvtComplete"));
				_isOK = false;

				}
			}
	}

	 

}
