﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ManageBuilding : MonoBehaviour {

	public List<Sprite> spriteList;

	private Transform[] childs;

	// Use this for initialization
	void Start () {
		childs = this.gameObject.GetComponentsInChildren<Transform>();

		foreach(Transform c in childs){
			c.gameObject.AddComponent<SpriteRenderer>();
			int i = Random.Range(0, spriteList.Count);
			c.gameObject.GetComponent<SpriteRenderer>().sprite = spriteList[i];
		}
		GameObject.Destroy(this.GetComponent<SpriteRenderer>());
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
