﻿using UnityEngine;
using System.Collections;

public class ClientBehaviour : MonoBehaviour {

	private bool IsFall;
	private bool IsCliked;
	private bool IsDead;

	private Animator anim;
	private GameObject taxi;
	private ParticleSystem ps;

	// Use this for initialization
	void Start () {
		IsDead = false;
		IsFall = false;
		IsCliked = false;
		ps = this.GetComponentInChildren<ParticleSystem>();
		taxi = GameObject.Find("TaxiTentacule");
		anim = this.GetComponentInChildren<Animator>();
	}

	public void setFall(bool b){
		this.IsFall = b;
		if(this.tag == "ClientA"){
			anim.Play("ClientA_Fall");
		}else if(this.tag == "ClientB"){
			anim.Play("ClientB_Fall");
		}else if(this.tag == "ClientC"){
			anim.Play("ClientC_Fall");
		}else{
			anim.Play("ClientD_Fall");
		}
	}

	public void setCliked(bool b){
		this.IsCliked = b;
	}


	void thisDie(){
		this.GetComponentInChildren<SpriteRenderer>().enabled = false;
		enabled = false;
		Destroy(this.gameObject);
	}

	// Update is called once per frame
	void Update () {
		if(IsFall && IsCliked){
			//31 le nombre de frame d'annimation de l'attrape
			this.transform.Translate(new Vector3((taxi.transform.position.x - this.transform.position.x)/31,
			                                     (taxi.transform.position.y - this.transform.position.y)/31,
			                                     0));
			if(IsDead == false){
				IsDead = true;
				ps.Play();
				Invoke("thisDie", 1);
			}
		}
	}


}
