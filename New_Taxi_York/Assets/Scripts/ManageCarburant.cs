﻿using UnityEngine;
using System.Collections;

public class ManageCarburant : MonoBehaviour {
	public GameObject fuel;
	public float diminutionParSec;
	public float augmentationBonus;

	private UISlider slider;
	// Use this for initialization
	void Start () {	
		slider = fuel.gameObject.GetComponent<UISlider> ();

	}

	public void augmentation(){
		if(slider != null)
			slider.sliderValue += augmentationBonus;
	}
	public bool getIsMax()
	{
		if (slider.sliderValue == 1F)
			return true;
		else
			return false;
		return false;
	}
	// Update is called once per frame
	void Update () {
		if(slider!=null)
			slider.sliderValue -= diminutionParSec * Time.deltaTime;
	}
}
